import QtQuick 2.1
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Entity {
    id: lights

    // Point Light (pulsing)
    Entity {
        components: [
            PointLight {
                color: "white"
                intensity: 0.3
                constantAttenuation: 1.0
                linearAttenuation: 0.0
                quadraticAttenuation: 0.0025

            },
            Transform {
                translation: Qt.vector3d(0.0, 5.0, 0.0)
            }
        ]
    }

    // 2 x Directional Lights (steady)
    Entity {
        components: [
            DirectionalLight {
                worldDirection: Qt.vector3d(0.3, 5.0, 0.0).normalized();
                color: "#fbf9ce"
                intensity: 0.3
            }
        ]
    }

    Entity {
        components: [
            DirectionalLight {
                worldDirection: Qt.vector3d(-0.3, 5.3, 0.0).normalized();
                color: "#9cdaef"
                intensity: 0.15
            }
        ]
    }

    // Spot Light (sweeping)
    Entity {
        components: [
            SpotLight {
                localDirection: Qt.vector3d(0.0, 7.0, -5.0)
                color: "white"
                intensity: 0.8
            },
            Transform {
                id: spotLightTransform
                translation: Qt.vector3d(0.0, 0.0, 5.0)

                }

        ]
    }
}
