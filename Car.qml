import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0
import QtQuick 2.0 as QQ2
import Qt3D.Extras 2.9
import QtQuick 2.13


Entity {
    id: carLambo
    property Component3D carMaterial: scene.colorChange? materialMetal : material
    components: [car, carMaterial,transform]
    Mesh {
        id: car
        source: "qrc:/models/huracan.obj"
    }

    Transform {
        id: transform

        rotationX: slid1.value
        rotationY: slid2.value
        rotationZ: slid3.value
        property real rotx: -5
        property real roty: -5
        property real rotz: -5

        translation: Qt.vector3d(rotx,roty,rotz)

    }
    PhongMaterial {
        id: material
        ambient: Qt.rgba(0.1,0.1,0.1,0.5)
        diffuse: "#030303"
        specular: "#fcba03"
        shininess: 50
    }
    MetalRoughMaterial {
        id: materialMetal
        baseColor: Qt.rgba( 0.8, 0.4, 0.0, 1.0 )
        metalness: 0.2
        roughness: 0.5
    }
    SequentialAnimation
    {
        loops: Animation.Infinite
        running: true
        NumberAnimation {
            target: transform
            property: "rotx"
            duration: 5000
            easing.type: Easing.InOutQuad
            from: -5
            to: 5

        }
        NumberAnimation {
            target: transform
            property: "rotx"
            duration: 5000
            easing.type: Easing.InOutQuad
            from: 5
            to: -5

        }
        NumberAnimation {
            target: transform
            property: "roty"
            duration: 5000
            easing.type: Easing.InOutQuad
            from: -5
            to: 5

        }
        NumberAnimation {
            target: transform
            property: "roty"
            duration: 5000
            easing.type: Easing.InOutQuad
            from: 5
            to: -5

        }
        NumberAnimation {
            target: transform
            property: "rotz"
            duration: 5000
            easing.type: Easing.InOutQuad
            from: -5
            to: 5

        }
        NumberAnimation {
            target: transform
            property: "rotz"
            duration: 5000
            easing.type: Easing.InOutQuad
            from: 5
            to: -5

        }

    }



}
