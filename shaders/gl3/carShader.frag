#version 150 core

uniform vec3 maincolor;
out vec4 fragColor;
in vec3 Normal;  

void main()
{
    //output color from material
    fragColor = clamp(dot(-vec4(0.5,0,0.5), maincolor), 0.0, 1.0);
}
