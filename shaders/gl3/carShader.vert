#version 150 core

in vec3 position;
out vec4 diffuseColor;

void main()
{
    vec2 col = (noise2(position.xy)*0.5)+0.5;
    float val = (col.x+col.y)/2.0;
    diffuseColor = gl_Color * vec4(val,val,val,0.5);
    diffuseColor.a = 1.0;
}
