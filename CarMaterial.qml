import Qt3D.Core 2.0
import Qt3D.Render 2.0

Material {
    id: root

    //! [0]
    property color maincolor: Qt.rgba(0.0, 0.0, 0.0, 1.0)

    parameters: [
        Parameter {
            name: "maincolor"
            value: Qt.vector3d(root.maincolor.r, root.maincolor.g, root.maincolor.b)
        }
    ]

    effect: Effect {

        property string vertex: "qrc:/shaders/gl3/carShader.vert"
        property string fragment: "qrc:/shaders/gl3/carShader.frag"


        FilterKey {
            id: forward
            name: "renderingStyle"
            value: "forward"
        }
        ShaderProgram {
            id: gl3Shader
            vertexShaderCode: loadSource(parent.vertex)
            fragmentShaderCode: loadSource(parent.fragment)
        }
        techniques: [

            Technique {
                filterKeys: [forward]
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 1
                }
                renderPasses: RenderPass {
                    shaderProgram: gl3Shader
                }
            }
        ]
    }
}
