/****************************************************************************
** Resource object code
**
** Created by: The Resource Compiler for Qt version 5.13.2
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

  // /home/iks/Documents/qtprojects/advancedcustommaterial/textures/foam.jpg
  
  
    // /home/iks/Documents/qtprojects/advancedcustommaterial/textures/sky.jpg
  
  
  
static const unsigned char qt_resource_data[842568] = { 'Q', 'R', 'C', '_', 'D', 'A', 'T', 'A' };

static const unsigned char qt_resource_name[] = {
  // textures
  0x0,0x8,
  0xc,0xfb,0xc7,0x83,
  0x0,0x74,
  0x0,0x65,0x0,0x78,0x0,0x74,0x0,0x75,0x0,0x72,0x0,0x65,0x0,0x73,
    // foam.jpg
  0x0,0x8,
  0x5,0x80,0x5c,0xc7,
  0x0,0x66,
  0x0,0x6f,0x0,0x61,0x0,0x6d,0x0,0x2e,0x0,0x6a,0x0,0x70,0x0,0x67,
    // sky.jpg
  0x0,0x7,
  0xa,0x2c,0x51,0x87,
  0x0,0x73,
  0x0,0x6b,0x0,0x79,0x0,0x2e,0x0,0x6a,0x0,0x70,0x0,0x67,
  
};

static const unsigned char qt_resource_struct[] = {
  // :
  0x0,0x0,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x1,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  // :/textures
  0x0,0x0,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x2,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  // :/textures/foam.jpg
  0x0,0x0,0x0,0x16,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x0,
0x0,0x0,0x1,0x70,0x7d,0xdb,0x5c,0x24,
  // :/textures/sky.jpg
  0x0,0x0,0x0,0x2c,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0xa,0x29,0x5c,
0x0,0x0,0x1,0x70,0x7d,0xdb,0x5c,0x30,

};

#ifdef QT_NAMESPACE
#  define QT_RCC_PREPEND_NAMESPACE(name) ::QT_NAMESPACE::name
#  define QT_RCC_MANGLE_NAMESPACE0(x) x
#  define QT_RCC_MANGLE_NAMESPACE1(a, b) a##_##b
#  define QT_RCC_MANGLE_NAMESPACE2(a, b) QT_RCC_MANGLE_NAMESPACE1(a,b)
#  define QT_RCC_MANGLE_NAMESPACE(name) QT_RCC_MANGLE_NAMESPACE2( \
        QT_RCC_MANGLE_NAMESPACE0(name), QT_RCC_MANGLE_NAMESPACE0(QT_NAMESPACE))
#else
#   define QT_RCC_PREPEND_NAMESPACE(name) name
#   define QT_RCC_MANGLE_NAMESPACE(name) name
#endif

#ifdef QT_NAMESPACE
namespace QT_NAMESPACE {
#endif

bool qRegisterResourceData(int, const unsigned char *, const unsigned char *, const unsigned char *);
bool qUnregisterResourceData(int, const unsigned char *, const unsigned char *, const unsigned char *);

#ifdef QT_NAMESPACE
}
#endif

int QT_RCC_MANGLE_NAMESPACE(qInitResources_textures)();
int QT_RCC_MANGLE_NAMESPACE(qInitResources_textures)()
{
    int version = 3;
    QT_RCC_PREPEND_NAMESPACE(qRegisterResourceData)
        (version, qt_resource_struct, qt_resource_name, qt_resource_data);
    return 1;
}

int QT_RCC_MANGLE_NAMESPACE(qCleanupResources_textures)();
int QT_RCC_MANGLE_NAMESPACE(qCleanupResources_textures)()
{
    int version = 3;
    QT_RCC_PREPEND_NAMESPACE(qUnregisterResourceData)
       (version, qt_resource_struct, qt_resource_name, qt_resource_data);
    return 1;
}

namespace {
   struct initializer {
       initializer() { QT_RCC_MANGLE_NAMESPACE(qInitResources_textures)(); }
       ~initializer() { QT_RCC_MANGLE_NAMESPACE(qCleanupResources_textures)(); }
   } dummy;
}
