/****************************************************************************
** Resource object code
**
** Created by: The Resource Compiler for Qt version 5.13.2
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

  // /home/iks/Documents/qtprojects/advancedcustommaterial/models/huracan.obj
  
  
    // /home/iks/Documents/qtprojects/advancedcustommaterial/models/plane-10x10.obj
  
  
  
static const unsigned char qt_resource_data[1589775] = { 'Q', 'R', 'C', '_', 'D', 'A', 'T', 'A' };

static const unsigned char qt_resource_name[] = {
  // models
  0x0,0x6,
  0x7,0x45,0xac,0x33,
  0x0,0x6d,
  0x0,0x6f,0x0,0x64,0x0,0x65,0x0,0x6c,0x0,0x73,
    // huracan.obj
  0x0,0xb,
  0x9,0xde,0xc5,0x6a,
  0x0,0x68,
  0x0,0x75,0x0,0x72,0x0,0x61,0x0,0x63,0x0,0x61,0x0,0x6e,0x0,0x2e,0x0,0x6f,0x0,0x62,0x0,0x6a,
    // plane-10x10.obj
  0x0,0xf,
  0xb,0xd2,0x81,0xca,
  0x0,0x70,
  0x0,0x6c,0x0,0x61,0x0,0x6e,0x0,0x65,0x0,0x2d,0x0,0x31,0x0,0x30,0x0,0x78,0x0,0x31,0x0,0x30,0x0,0x2e,0x0,0x6f,0x0,0x62,0x0,0x6a,
  
};

static const unsigned char qt_resource_struct[] = {
  // :
  0x0,0x0,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x1,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  // :/models
  0x0,0x0,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x2,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  // :/models/huracan.obj
  0x0,0x0,0x0,0x12,0x0,0x1,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x0,
0x0,0x0,0x1,0x69,0x45,0x24,0x73,0x3a,
  // :/models/plane-10x10.obj
  0x0,0x0,0x0,0x2e,0x0,0x1,0x0,0x0,0x0,0x1,0x0,0x18,0x38,0xb5,
0x0,0x0,0x1,0x6d,0xec,0xe4,0x31,0xe8,

};

#ifdef QT_NAMESPACE
#  define QT_RCC_PREPEND_NAMESPACE(name) ::QT_NAMESPACE::name
#  define QT_RCC_MANGLE_NAMESPACE0(x) x
#  define QT_RCC_MANGLE_NAMESPACE1(a, b) a##_##b
#  define QT_RCC_MANGLE_NAMESPACE2(a, b) QT_RCC_MANGLE_NAMESPACE1(a,b)
#  define QT_RCC_MANGLE_NAMESPACE(name) QT_RCC_MANGLE_NAMESPACE2( \
        QT_RCC_MANGLE_NAMESPACE0(name), QT_RCC_MANGLE_NAMESPACE0(QT_NAMESPACE))
#else
#   define QT_RCC_PREPEND_NAMESPACE(name) name
#   define QT_RCC_MANGLE_NAMESPACE(name) name
#endif

#ifdef QT_NAMESPACE
namespace QT_NAMESPACE {
#endif

bool qRegisterResourceData(int, const unsigned char *, const unsigned char *, const unsigned char *);
bool qUnregisterResourceData(int, const unsigned char *, const unsigned char *, const unsigned char *);

#if defined(__ELF__) || defined(__APPLE__)
static inline unsigned char qResourceFeatureZlib()
{
    extern const unsigned char qt_resourceFeatureZlib;
    return qt_resourceFeatureZlib;
}
#else
unsigned char qResourceFeatureZlib();
#endif

#ifdef QT_NAMESPACE
}
#endif

int QT_RCC_MANGLE_NAMESPACE(qInitResources_models)();
int QT_RCC_MANGLE_NAMESPACE(qInitResources_models)()
{
    int version = 3;
    QT_RCC_PREPEND_NAMESPACE(qRegisterResourceData)
        (version, qt_resource_struct, qt_resource_name, qt_resource_data);
    return 1;
}

int QT_RCC_MANGLE_NAMESPACE(qCleanupResources_models)();
int QT_RCC_MANGLE_NAMESPACE(qCleanupResources_models)()
{
    int version = 3;
    version += QT_RCC_PREPEND_NAMESPACE(qResourceFeatureZlib());
    QT_RCC_PREPEND_NAMESPACE(qUnregisterResourceData)
       (version, qt_resource_struct, qt_resource_name, qt_resource_data);
    return 1;
}

namespace {
   struct initializer {
       initializer() { QT_RCC_MANGLE_NAMESPACE(qInitResources_models)(); }
       ~initializer() { QT_RCC_MANGLE_NAMESPACE(qCleanupResources_models)(); }
   } dummy;
}
