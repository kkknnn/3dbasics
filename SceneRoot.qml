import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.12
import Qt3D.Input 2.0
import QtQuick 2.0 as QQ2
import QtQuick 2.13
import Qt3D.Input 2.0

Entity {
    id: sceneRoot

    Camera {
        id: camera
        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        nearPlane: 0.1
        farPlane: 1000.0
        position: Qt.vector3d(0.0, 4.0, 15.0)
        upVector: Qt.vector3d(0.0, 1.0, 0.0)
        viewCenter: Qt.vector3d(0.0, -2.0, 0.0)
    }

    FirstPersonCameraController {
        camera: camera
    }

   //Lights{}

    components: [
        RenderSettings {
            activeFrameGraph: ForwardRenderer {
                id: renderer
                clearColor: "black"
                camera: camera
            }
        },
        InputSettings { }
    ]
    Entity {
           components: [
               PointLight {
                   color: "white"
                   intensity: 3
                   constantAttenuation: 1.0
                   linearAttenuation: 0.0
                   quadraticAttenuation: 0.0025


               },
               Transform {
                   translation: Qt.vector3d(0.0, -5.0, -5.0)
               }
           ]
       }
//    Entity {
//           components: [
//               DirectionalLight {
//                   color: "white"
//                   intensity: 0.4
//                   worldDirection: Qt.vector3d(0.0, 0.0, 3.0).normalized();


//               },
//               Transform {
//                   translation: Qt.vector3d(0.0, 0.0, 0.0)
//               }
//           ]
//       }
    Entity {
        id: floor

        components: [
            Mesh {
                source: "qrc:/models/plane-10x10.obj"
            },



            DiffuseSpecularMaterial {
                diffuse: TextureLoader {
                    source: "qrc:/textures/sky.jpg"
                }

            }
            ,

            Transform {
                translation: Qt.vector3d(0, -2, 0)

            }

        ]

        Texture2D{
            id: texture
            TextureImage {
                source: "qrc:/textures/sky.jpg"

            }
        }
    }
    Entity
    {
          components: [
        ExtrudedTextMesh
        {
            id: textMesh
            text: "HELLO"
            depth: 0.9
        },
        DiffuseSpecularMaterial {
            diffuse: TextureLoader {
                source: "qrc:/textures/foam.jpg"
            }
        },

        Transform {
            id: textTransform
            property int rotX: 0
            translation: Qt.vector3d(0, 0, -20)
            rotationY: rotX



            SequentialAnimation
            {
                loops: Animation.Infinite
                running: true
                NumberAnimation {
                    target: textTransform
                    property: "rotX"
                    duration: 5000
                    easing.type: Easing.InOutQuad
                    from: -180
                    to: 180

                }
                NumberAnimation {
                    target: textTransform
                    property: "rotX"
                    duration: 5000
                    easing.type: Easing.InOutQuad
                    from: 180
                    to: -180

                }

            }


        }
          ]

    }

    Entity
    {
        components: [
            Mesh {
                source: "qrc:/models/plane-10x10.obj"
            },



            DiffuseSpecularMaterial {
                diffuse: TextureLoader {
                    source: "qrc:/textures/sky.jpg"
                }

            }
            ,

            Transform {
                translation: Qt.vector3d(0, 0, -10)
                rotationX: 90

            }

        ]


    }


    Car { }
}
