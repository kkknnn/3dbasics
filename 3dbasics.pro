TEMPLATE = app

!include( ../examples.pri ) {
    error( "Couldn't find the examples.pri file!" )
}

QT += qml quick quickcontrols2 3dcore
QT += 3dextras

CONFIG += c++14
CONFIG += resources_big

SOURCES += main.cpp

RESOURCES += qml.qrc \
            models.qrc \
            textures.qrc
