import QtQuick 2.0
import QtQuick.Scene3D 2.0
import Qt3D.Render 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Material 2.0


ApplicationWindow {
    id: window
    width: 800
    height: 480
    visible: true
    title: "Qt3d example"
    Material.theme: Material.Dark
    Material.accent: Material.DeepOrange
    Material.primary: Material.Orange
    Material.foreground: "#eda618"
    Rectangle {
        id: scene
        property bool colorChange: true
        anchors.fill: parent
        color: "#2d2d2d"
        Rectangle {
            id: controlsbg
            anchors.fill: parent
            anchors.leftMargin: 10
            anchors.topMargin: 10
            anchors.rightMargin: 1720
            anchors.bottomMargin: 10
            color: "grey"
            Column {
                anchors.fill: parent
                anchors.leftMargin: 5
                anchors.topMargin: 5
                spacing: 10
                Rectangle {
                    id: carRotX
                    width: 180
                    height: 60
                    color: "#2d2d2d"
                    Text {
                        id: carRotXText
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        text: "CarRotation"
                        color: "white"
                        font.bold: true
                        font.pointSize: 12
                    }
                    Slider {
                        id: slid1
                        anchors.fill: parent
                        anchors.topMargin: 30
                        anchors.rightMargin: 10
                        anchors.leftMargin: 10
                        value: 0.0
                        maximumValue: 180.0
                        minimumValue: -180.0
                    }
                }
                Rectangle {
                    id: carRotY
                    width: 180
                    height: 60
                    color: "#2d2d2d"
                    Text {
                        id: carRotYtext
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        text: "CarRotation"
                        color: "white"
                        font.bold: true
                        font.pointSize: 12
                    }
                    Slider {
                        id: slid2
                        anchors.fill: parent
                        anchors.topMargin: 30
                        anchors.rightMargin: 10
                        anchors.leftMargin: 10
                        value: 0.0
                        maximumValue: 180.0
                        minimumValue: -180.0
                    }
                }
                Rectangle {
                    id: carRotZ
                    width: 180
                    height: 60
                    color: "#2d2d2d"
                    Text {
                        id: carRotZtext
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        text: "CarRotation"
                        color: "white"
                        font.bold: true
                        font.pointSize: 12
                    }
                    Slider {
                        id: slid3
                        anchors.fill: parent
                        anchors.topMargin: 30
                        anchors.rightMargin: 10
                        anchors.leftMargin: 10
                        value: 0.0
                        maximumValue: 180.0
                        minimumValue: -180.0
                    }
                }
                Rectangle {
                    id: materialChange
                    width: 180
                    height: 60
                    Button
                    {
                        id:materialChangeButton
                        anchors.fill: parent
                        Material.background: "#404244"
                        text: "Change Material"
                        onClicked:
                        {
                        if(scene.colorChange)
                        {
                            scene.colorChange = false
                        }
                        else
                        {
                            scene.colorChange = true

                        }

                        }
                    }

                }
            }
        }

        Scene3D {
            id: scene3d
            anchors.fill: parent
            anchors.leftMargin: 200
            anchors.topMargin: 10
            anchors.rightMargin: 10
            anchors.bottomMargin: 10
            focus: true
            aspects: ["input", "logic"]
            cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

            SceneRoot {
                id: root
            }
        }
    }
}
